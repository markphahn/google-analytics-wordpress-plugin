# Simple Google Analytics Plugin

So you want to add Google Analytics to your Wordpress site and don't want
it to get overwritten with the next update.

The quickest way is to create a simple plugin. This is that plugin.

## Approach

The idea is to build a 20-30 line plugin which you manage. The plugin 
is not very complicated and easy if you have coding skills.

My aproach works is you have access to the filesystem and can add the
plugin file to the Wordpress folder. (I could upskill this to be a 
marketplace solution, but I don't want to ahve to create a admin UI.)

# Step by Setp Directions

## Google Analytics
Sign up for Google Analytics

Find the tracking code snippit. It somewhere under the
admin UI. It will look a lot like this:

![GoogleAnalyticsTrackingCode.png](GoogleAnalyticsTrackingCode.png)

## Create the plugin file

In the directory `wp_content/plugins` create a file for your plugin.
Name it with your initials followed by `-google-analytics.php`. In my
case my initials are MpH so I called the file `mph-google-analytics.php`.

The reason to pre-pend your initials is that the file name is the 
identification for the plugin. As in global identification. If you choose
a filename that matches another plugin's name on the marketplace, you may
see notices that there's a newer version of your plugin available. (You don't
what that confusion, choose a unique name.)

## Add the header comments

You need the speciallly formatted comments so that Wordpress can get
the metadata about the application.

## Insert the "hook" which adds the Google analytics scripts

The code `add_action('wp_head', 'wpb_add_googleanalytics');` will
cal the `wpb_add_googleanalytics` function whenever the HTML `<head>` 
section is being output. 

If you are already PHP-knowledgeable you can read the 
`wpb_add_googleanalytics` function.

This function has the PHP `?>` closing tag. However the HTML that
follows is still "swallowed" by the function defintion (a bit like
a HEREDOC). The last line includes the PHP start tag, `<?php` then
the end of the function.

When the function is called it will emit the HTML that was swallowed.
This is normal PHP, but hard for me to grok at first read.

## Paste in your Google Analytics script

From the earlier step where Google Analytics show you the 
tracking code, copy that code into the of this function (After
the PHP `?>` closing tag and before the the PHP start tag, `<?php`.
