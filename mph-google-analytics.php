<?php
/**                                                                                                                                                      
 * @package Add_Google_Analytics_MpH                                                                                                                     
 * @version 0.9                                                                                                                                          
 */
/*                                                                                                                                                       
Plugin Name: Add Google Analytics MpH                                                                                                                    
Plugin URI: https://gitlab.com/markphahn/google-analytics-wordpress-plugin/                                                                              
Description: Very, very simple plugin to paste in Google Analytics tracking code                                                                         
Author: Mark Hahn                                                                                                                                        
Version: 0.9                                                                                                                                             
Author URI: http://www.tcbtech.com
License: BSD-3-Clause
License URI: https://directory.fsf.org/wiki/License:BSD-3-Clause
*/

add_action('wp_head', 'wpb_add_googleanalytics');
function wpb_add_googleanalytics() { ?>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-XXXXXXXX-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-XXXXXXXX-1');
</script>



<?php } ?>



